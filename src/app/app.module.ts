import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AbsencesComponent } from './components/absences/absences.component';
import { RetraitesComponent } from './components/retraites/retraites.component';
import { AvantagesComponent } from './components/avantages/avantages.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CongesComponent } from './components/conges/conges.component';
import { AjouterCongeComponent } from './components/forms/ajouter-conge/ajouter-conge.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

import {ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import { NavbarHeaderComponent } from './components/navbar-header/navbar-header.component';
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AjouterDiplomeComponent } from './components/forms/ajouter-diplome/ajouter-diplome.component';
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import { ModifierCongeComponent } from './components/forms/modifier-conge/modifier-conge.component';
import { SupportFormComponent } from './components/forms/support-form/support-form.component';
import {MatNativeDateModule} from "@angular/material/core";
import { SettingsComponent } from './components/settings/settings.component';
import {MatSnackBar, MatSnackBarModule} from "@angular/material/snack-bar";
import { LoginComponent } from './components/login/login.component';
import {MatIconModule} from "@angular/material/icon";
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { JustifAbsenceComponent } from './components/forms/justif-absence/justif-absence.component';
import { AjouterRetourComponent } from './components/forms/ajouter-retour/ajouter-retour.component';
import { ModifierJustifAbsenceComponent } from './components/forms/modifier-justif-absence/modifier-justif-absence.component';

@NgModule({
  declarations: [
    AppComponent,
    AbsencesComponent,
    RetraitesComponent,
    AvantagesComponent,
    HomeComponent,
    ProfileComponent,
    CongesComponent,
    AjouterCongeComponent,
    NavbarHeaderComponent,
    AjouterDiplomeComponent,
    ModifierCongeComponent,
    SupportFormComponent,
    SettingsComponent,
    LoginComponent,
    ForgetPasswordComponent,
    JustifAbsenceComponent,
    AjouterRetourComponent,
    ModifierJustifAbsenceComponent
  ],
  entryComponents:[AjouterDiplomeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule, MatSnackBarModule, MatIconModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
