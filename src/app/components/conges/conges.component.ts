import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {CongesService} from "../../services/conges/conges.service";
import {Conges} from "../../models/Conges";
import {Salarie} from "../../models/Salarie";
import {AjouterCongeComponent} from "../forms/ajouter-conge/ajouter-conge.component";
import {MatDialog,MatDialogConfig} from "@angular/material/dialog";
import {ModifierCongeComponent} from "../forms/modifier-conge/modifier-conge.component";
import {ProfileService} from "../../services/profile/profile.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AjouterRetourComponent} from "../forms/ajouter-retour/ajouter-retour.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";



@Component({
  selector: 'app-conges',
  templateUrl: './conges.component.html',
  styleUrls: ['./conges.component.css']
})
export class CongesComponent implements OnInit {

  public conges:Conges[];
  public formAddConge:boolean = false;
  formModifyConge: boolean= false;
  public salarie :Salarie;
  notAccepted:boolean =false;
  private newConge: Conges;
  constructor(private _snackBar :MatSnackBar , private profileService:ProfileService,private router: Router, private congesService:CongesService , private dialog:MatDialog) { }


  ngOnInit(): void {
    this.congesService.refreshNeeds$.subscribe(() =>{
      this.getConges();
    })

    this.getConges();
    this.getSalarie();
  }

  openSnackBar(snackBarmessage,snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction,{
      duration: 2000,
    });
  }


  getSalarie(){
    this.profileService.getProfile().subscribe(profile => {
      this.salarie = <Salarie>profile;
    })  }


  getConges(){
    this.congesService.getAllConges().subscribe(data=>{
      // @ts-ignore
      this.conges = data;

      console.log(data);
      this.salarie=this.conges[0].salarie;
    },error =>{
      console.error(error);
    })
  }
  deleteConge(id) {
    let conf = confirm("Êtes-vous sûr ?");
    if(conf){
      this.congesService.deleteConge(id).subscribe(data=>{
        // this.getConges();
        this.openSnackBar("Votre congé est supprimée avec succès","undo");

      },error => {
        console.log(error);

      })}
  }

  openDialogModify(conge) {
    // this.formModifyConge = true;
    // this.router.navigateByUrl("/conges/"+id+"/modifier");
    // this.router.navigateByUrl("/conges/"+id);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = {
      title: 'Modifier Conge',
      conge : conge,
      reload : this.getConges(),
      salarie : this.salarie,
    };
    this.dialog.open(ModifierCongeComponent,dialogConfig);
    this.router.navigateByUrl("/conges");
  }


  openDialogCreate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Ajouter Conge',
      reload : this.getConges(),
      salarie :this.salarie

    };
    this.dialog.open(AjouterCongeComponent,dialogConfig);
    this.router.navigateByUrl("/conges");

  }

  openDialogRetour(conge) {
    this.congesService.getOneConge(conge.id).subscribe(response =>{
      conge =<Conges> response;
    },error => {
      console.log(error);
    })
    const dialogConfig = new MatDialogConfig();
    // console.log(conge)
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = {
      title: 'Ajouter date de retour',
      conge : conge,
      // reload : this.getConges(),
      salarie : this.salarie,
    };
    this.dialog.open(AjouterRetourComponent,dialogConfig);
    this.router.navigateByUrl("/conges");
  }
}
