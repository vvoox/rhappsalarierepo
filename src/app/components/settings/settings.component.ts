import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Salarie} from "../../models/Salarie";
import {ProfileService} from "../../services/profile/profile.service";
import {SettingsService} from "../../services/settings/settings.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  passwordWrongError:boolean=false;
  passwordWrongmessage:string;
  passwordMatchError:boolean=false ;
  saved: boolean = false;

  salarie?: Salarie;

  passwordFromGroup = new FormGroup({
    password: new FormControl('', Validators.required),
    newPassword : new FormControl('', [Validators.required,Validators.minLength(8)]),
    confPassword : new FormControl('',[Validators.required,Validators.minLength(8)] )
  })

  constructor( private profileService:ProfileService , private settingsService:SettingsService) {

  }

  ngOnInit(): void {
    this.profileService.getProfile().subscribe(profile => {
      // @ts-ignore
      this.salarie=profile;

    })
  }

  checkPasswords(){
    return this.passwordFromGroup.value.newPassword == this.passwordFromGroup.value.confPassword ? this.sendNewPassword() : this.passwordMatchError=true;
  }
  sendNewPassword(){
    this.passwordMatchError=false;
    this.salarie.password=this.passwordFromGroup.value.password;
    this.salarie.newPassword=this.passwordFromGroup.value.newPassword;
    console.log(this.salarie);
  this.settingsService.modifyPassword(this.salarie).subscribe(response => {
    this.passwordWrongError=false;
    this.saved=true;

  },error => {
    console.log(error.error.message);
    this.passwordWrongmessage = error.error.message;
    this.passwordWrongError=true;
    this.passwordMatchError=false;
  })
  }
}
