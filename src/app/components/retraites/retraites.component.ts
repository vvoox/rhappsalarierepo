import { Component, OnInit } from '@angular/core';
import {RetraitesService} from "../../services/retraite/retraites.service";
import {Retraites} from "../../models/Retraites";
import {Salarie} from "../../models/Salarie";
import {ProfileComponent} from "../profile/profile.component";

@Component({
  selector: 'app-retraites',
  templateUrl: './retraites.component.html',
  styleUrls: ['./retraites.component.css']
})
export class RetraitesComponent implements OnInit {

  public retraites:Retraites;

  constructor( private retaitesService:RetraitesService) { }

  ngOnInit(): void {
    this.getRetaites();
  }

  getRetaites(){
    this.retaitesService.getAllRetraites().subscribe(data =>{
      // @ts-ignore
      this.retraites=data;

    },error => {
      console.log(error);
    })
  }

}
