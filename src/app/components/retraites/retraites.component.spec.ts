import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetraitesComponent } from './retraites.component';

describe('RetraitesComponent', () => {
  let component: RetraitesComponent;
  let fixture: ComponentFixture<RetraitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetraitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetraitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
