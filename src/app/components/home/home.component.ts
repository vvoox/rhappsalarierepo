import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {CongesComponent} from "../conges/conges.component";
import {Salarie} from "../../models/Salarie";
import {ProfileComponent} from "../profile/profile.component";
import {ProfileService} from "../../services/profile/profile.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {AjouterCongeComponent} from "../forms/ajouter-conge/ajouter-conge.component";
import {SupportFormComponent} from "../forms/support-form/support-form.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private congestComponent:CongesComponent ;
  public salarie:Salarie;

  constructor(private router:Router , private profileService:ProfileService , private dialog:MatDialog) { }

  ngOnInit(): void {
    this.profileService.getProfile().subscribe(profile => {
      // @ts-ignore
      this.salarie=profile;
      console.log(this.salarie);
    })
  // congesComponent:CongesComponent;
  }

  // ajouterConge() {
  //   // this.router.navigateByUrl("/conges");
  //   this.congestComponent.addConge();
  // }

  // ajouterAbsence() {
  //   this.router.navigateByUrl("/ajouter-absence");
  // }

  voirRetraites(){
    this.router.navigateByUrl("/retraites");
  }
  voirAvantages(){
    this.router.navigateByUrl("/avantages");
  }

  voirAbsence() {
    this.router.navigateByUrl("/absences");

  }
  voirConge() {
    this.router.navigateByUrl("/conges");

  }

  helpMe() {
    console.log(this.salarie)
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = {
      salarie: this.salarie,
    };
    this.dialog.open(SupportFormComponent,dialogConfig);
    this.router.navigateByUrl("/home");
  }
}
