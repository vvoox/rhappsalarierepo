import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AbsencesService} from "../../services/absence/absences.service";
import {Absences} from "../../models/Absences";
import {ProfileComponent} from "../profile/profile.component";
import {Salarie} from "../../models/Salarie";
import {ProfileService} from "../../services/profile/profile.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ModifierCongeComponent} from "../forms/modifier-conge/modifier-conge.component";
import {JustifAbsenceComponent} from "../forms/justif-absence/justif-absence.component";
import {ModifierJustifAbsenceComponent} from "../forms/modifier-justif-absence/modifier-justif-absence.component";
import {DomSanitizer} from "@angular/platform-browser";


@Component({
  selector: 'app-absences',
  templateUrl: './absences.component.html',
  styleUrls: ['./absences.component.css']
})
export class AbsencesComponent implements OnInit {

  public absences:Absences[];
  profile:ProfileComponent;
  public salarie:Salarie;
  private fileJustif: any;
  constructor(private sanitizer: DomSanitizer ,private dialog : MatDialog, private router:Router , private absencesService:AbsencesService,private profileService:ProfileService){ }

  ngOnInit(): void {

    this.absencesService.refreshNeeds$.subscribe(() =>{
      this.getAllAbsences();
    })
    this.getAllAbsences();
    this.getSalarie();
    // @ts-ignore
  }
  getSalarie(){
    this.profileService.getProfile().subscribe(profile => {
      this.salarie=<Salarie>profile;
      console.log(profile);
    })

  }

  getAllAbsences(){
    this.absencesService.getAbsences().subscribe(data=>{
      // console.log(data);
      // @ts-ignore
      this.absences=data;
      // console.log(sa);
      // this.salarie = this.absences[0].salarie;
    },error => {
      // console.log(error);
    })
  }

  // addAbsence() {
  //   this.router.navigateByUrl("/ajouter-absence");
  // }


  openDialogAbsence(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '800px';
    dialogConfig.data = {
      title: 'Add Justif',
      salarie : this.salarie,
      absenceId : id,
    };
    this.dialog.open(JustifAbsenceComponent,dialogConfig);
    // this.router.navigateByUrl("/Absences");
  }
  openDialogModifyJustif(id) {
    this.getSalarie();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '800px';
    dialogConfig.data = {
      title: 'Add Justif',
      salarie : this.salarie,
      absenceId : id,
    };
    this.dialog.open(ModifierJustifAbsenceComponent,dialogConfig);
    // this.router.navigateByUrl("/Absences");
  }


  downloadJustification(absence: Absences){
    this.absencesService.downloadJustification(absence.justificatif).subscribe(response => {
      this.fileJustif = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(response));
      console.log(this.fileJustif);
      var file = document.createElement("a");
      // file.download = "Justification "+absence.id;
      file.download = "Justification "+absence.id+".pdf";
      file.href = this.fileJustif.changingThisBreaksApplicationSecurity;
      file.click();
    },error => {
      console.log(error);
    })
  }
}
