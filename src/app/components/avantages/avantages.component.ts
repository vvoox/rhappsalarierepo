import { Component, OnInit } from '@angular/core';
import {AvantagesService} from "../../services/avantages/avantages.service";
import {Avantages} from "../../models/Avantages";
import {Salarie} from "../../models/Salarie";

@Component({
  selector: 'app-avantages',
  templateUrl: './avantages.component.html',
  styleUrls: ['./avantages.component.css']
})
export class AvantagesComponent implements OnInit {

  public avantages:Avantages[];
  public salarie:Salarie;

  constructor(private avantagesService:AvantagesService ) { }

  ngOnInit(): void {

    this.getAllAvantages();
  }

  getAllAvantages(){
    this.avantagesService.getAllAvantages().subscribe(data => {
      console.log(data);
      // @ts-ignore
      this.avantages = data;
      this.salarie = this.avantages[0].salarie;
    },error =>{
      console.error(error);
    })
  }

}
