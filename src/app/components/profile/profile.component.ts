import {Component, OnInit, SystemJsNgModuleLoader} from '@angular/core';
import {ProfileService} from "../../services/profile/profile.service";
import {Salarie} from "../../models/Salarie";
import {Router} from "@angular/router";
import {MatDialog,MatDialogConfig} from "@angular/material/dialog";
import {AjouterCongeComponent} from "../forms/ajouter-conge/ajouter-conge.component";
import {AjouterDiplomeComponent} from "../forms/ajouter-diplome/ajouter-diplome.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Poste} from "../../models/Poste";
import { DomSanitizer } from '@angular/platform-browser';
import {Diplomes} from "../../models/Diplomes";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SettingsService} from "../../services/settings/settings.service";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public salarie :Salarie;
  selectedImage :File;
  selectedCv:File;
  selectedDiplome:File;
  dateCv;
  nameCv:string;
  path:string;
  errorMessageCv:string;
  errorMessageImage:string;
  errorMessageDiplome:string;
  diplomeName : string;
  profileImage:any;

  contactFromGroup: FormGroup;
  userFromGroup : FormGroup;
  familyFormGroup :FormGroup;
  urgContactFromGroup : FormGroup;


  childShowUp:boolean=false;
  diplomeIsPDF:boolean;
  buttonDeleteImage:boolean = true;
  fileCv:any;
  fileDiplome:any;
  salarieImage:any;
  passwordWrongError:boolean=false;
  passwordWrongmessage:string;
  passwordMatchError:boolean=false ;
  saved: boolean = false;

  passwordFromGroup = new FormGroup({
    password: new FormControl('', Validators.required),
    newPassword : new FormControl('', [Validators.required,Validators.minLength(8)]),
    confPassword : new FormControl('',[Validators.required,Validators.minLength(8)] )
  })


  situations = ['Célibataire' , 'Marié(e)', 'Divorcé(e)'];

  constructor(private settingsService:SettingsService ,private _snackBar :MatSnackBar , private sanitizer: DomSanitizer,private profileService:ProfileService , private router:Router , private dialog:MatDialog) { }

  ngOnInit(): void {
    this.getProfile();
    this.dateCv=new Date();
  }

  refresh(){
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate([ProfileComponent]);
    });
  }

  openSnackBar(snackBarmessage,snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction,{
      duration: 2000,
    });
  }




  public getProfile(){
    this.profileService.getProfile().subscribe(data => {

      // @ts-ignore
      this.salarie = data;
      console.log(this.salarie)

      this.nameCv = this.salarie.cv;
      if(this.salarie.photo != null){this.buttonDeleteImage=false;}
      console.log(data);
      this.router.navigateByUrl("/profile");
      if(this.salarie.photo != null){this.getCurrentImage();}

      this.userFromGroup = new FormGroup({
        nom: new FormControl(this.salarie.nom, Validators.required),
        prenom: new FormControl(this.salarie.prenom, Validators.required),
        email: new FormControl(this.salarie.email, Validators.email),
        telephone: new FormControl(this.salarie.telephone, Validators.required),
      })

      this.contactFromGroup = new FormGroup({
        dateNaissance: new FormControl(this.salarie.dateNaissance.substring(0,10), Validators.required),
        lieuNaissance: new FormControl(this.salarie.lieuNaissance.toUpperCase(), Validators.required),
        adresse: new FormControl(this.salarie.adresse, Validators.required)
      })

      this.familyFormGroup = new FormGroup({
        etatFamiliale: new FormControl(this.salarie.etatFamiliale,Validators.required),
        nmbEnf: new FormControl(this.salarie.nmbEnf),
      })
      this.urgContactFromGroup = new FormGroup({
        cinUrg: new FormControl(this.salarie.cinUrg, Validators.required),
        nomUrg: new FormControl(this.salarie.nomUrg, Validators.required),
        prenomUrg: new FormControl(this.salarie.prenomUrg, Validators.required),
        emailUrg: new FormControl(this.salarie.emailUrg, Validators.email),
        telephoneUrg: new FormControl(this.salarie.telephoneUrg, Validators.required),
        adresseUrg: new FormControl(this.salarie.adresseUrg, Validators.required),
      })
    },error => {
      console.log(error);
    })
  }

  downloadCv(){
    this.profileService.downloadCv(this.salarie.cv).subscribe(response => {
      this.fileCv = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(response));
      // window.location.href =this.cv.changingThisBreaksApplicationSecurity;
      var file = document.createElement("a");
      file.download = this.salarie.cv;
      file.href = this.fileCv.changingThisBreaksApplicationSecurity;
      file.click();

    },error => {
      console.log(error);
    })
  }

  downloadDiplome(diplome) {
    console.log(diplome);
    this.profileService.downloadDiplome(diplome.path).subscribe(response => {
      this.fileDiplome = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(response));
      console.log(this.fileDiplome);
      var file = document.createElement("a");
      file.download = "diplome_"+diplome.id;
      file.href = this.fileDiplome.changingThisBreaksApplicationSecurity;
      file.click();
    },error => {
      console.log(error);
    })
  }

  getCurrentImage(){
    this.profileService.downloadImage(this.salarie.photo).subscribe(respo => {
      // console.log(respo);
      // this.profileImage = window.URL.createObjectURL(respo);
      console.log(this.salarie.photo);
      console.log(respo);
      let objectURL = URL.createObjectURL(respo);
      this.profileImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      // this.createImageFromBlob(respo);
    },error => {
      console.log(error);
    })
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.profileImage = reader.result;
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }


  selectPhoto($event: Event) {
    // @ts-ignore
    this.selectedImage = $event.target.files[0];
    console.log(this.selectedImage);
  }

  selectCv($event: Event) {
    // @ts-ignore
    this.selectedCv = $event.target.files[0];
    console.log(this.selectedCv);
  }

  selectDiplome($event: Event) {
    // @ts-ignore
    this.selectedDiplome = $event.target.files[0];
    // @ts-ignore
    this.diplomeName = $event.target.files[0].name;
    this.diplomeIsPDF = this.selectedDiplome.type.split("/")[1] == "pdf";
    console.log(this.diplomeIsPDF);
    this.errorMessageDiplome = "Format incorrect !";

  }


  onUploadImage() {
    this.profileService.uploadImage(this.selectedImage).subscribe(response =>{
      console.log(response);
      this.openSnackBar("La photo est chargée avec succès","undo");
      this.refresh();
      // this.buttonDeleteImage=true;
    },error => {
      console.log(error);
      this.errorMessageImage = error.error.message;
    })
  }

  onUploadCv() {
    this.profileService.uploadCv(this.selectedCv).subscribe(response =>{
      console.log(response);
      this.openSnackBar("Votre CV est chargée avec succès","undo");
      this.getProfile();
    },error => {
      console.log(error.error.message);
      this.errorMessageCv = error.error.message;
    })
  }

  // onUploadDiplome() {
  //   this.profileService.uploadCv(this.selectedDiplome).subscribe(response =>{
  //     console.log(response);
  //   },error => {
  //     console.log(error.error.message);
  //     this.errorMessageCv = error.error.message;
  //     this.getProfile();
  //
  //   })
  // }


  onOpenDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Ajouter Diplome',
      file : this.selectedDiplome,
    };
    const dialogRef = this.dialog.open(AjouterDiplomeComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log( 'Dialog was closed')
      this.getProfile();

      console.log(result)
    });
  }


  deleteImage() {
    let conf = confirm("Êtes-vous sûr ?");
    if(conf){
      this.profileService.deleteImage().subscribe(response =>{
        console.log(response);
        this.openSnackBar("La photo est chargée avec succès","undo");

        this.refresh();

      },error => {
        console.log(error);
      })
    }
  }
  deleteCv() {
    let conf = confirm("Êtes-vous sûr ?");
    if(conf){
    this.profileService.deleteCv().subscribe(response =>{
      console.log(response);
      this.openSnackBar("Votre Cv est supprimée avec succès","undo");

      this.getProfile();

    },error => {
      console.log(error);
    })
    }
  }


  deleteDiplome(id: number) {

    let conf = confirm("Êtes-vous sûr ?");
    if(conf){
    this.profileService.deleteDiplome(id).subscribe(response => {
      console.log(response);
      this.openSnackBar("Votre diplome est supprimée avec succès","undo");

      this.getProfile();
    },error => {
      console.log(error);
    })}
  }


  turnOnChildInput() {
    console.log(this.familyFormGroup.value);
    if(this.familyFormGroup.value.etatFamiliale != 'Célibataire'){
      this.childShowUp=true;
    }
    else {
      this.familyFormGroup.value.nmbEnf=0;
    }
  }

  // *************************************** Editing Profile Info **************************************************
  modifyerrorUser:boolean =true;
  modifyerrorContacts:boolean =true;
  modifyerrorSituation:boolean =true;
  modifyerrorUrgent:boolean =true;


  modifyUserSettings() {
    console.log(this.userFromGroup.value);
    this.salarie.nom = this.userFromGroup.value.nom;
    this.salarie.prenom = this.userFromGroup.value.prenom;
    this.salarie.email = this.userFromGroup.value.email;
    this.salarie.telephone = this.userFromGroup.value.telephone;
    console.log(this.salarie);
    this.profileService.modifyProfileUser(this.salarie).subscribe(response =>{
      console.log(response);
      this.modifyerrorUser=false;
    },error => {
      console.log(error);
      this.modifyerrorUser=true;

    })
  }

  modifyContactSettings() {
    this.salarie.dateNaissance = this.contactFromGroup.value.dateNaissance;
    this.salarie.lieuNaissance = this.contactFromGroup.value.lieuNaissance;
    this.salarie.adresse = this.contactFromGroup.value.adresse;
    this.profileService.modifyProfileContacts(this.salarie).subscribe(response =>{
      console.log(response);
      console.log(this.salarie);
      this.openSnackBar("Enregistré avec succès","undo");
      this.modifyerrorContacts=false;

    },error => {
      console.log(error);
      this.modifyerrorContacts=true;

    })
  }

  modifyFamilySettings() {
    console.log(this.familyFormGroup.value);
    this.salarie.etatFamiliale = this.familyFormGroup.value.etatFamiliale;
    this.salarie.nmbEnf = this.familyFormGroup.value.nmbEnf;
    this.profileService.modifyProfileContacts(this.salarie).subscribe(response =>{
      console.log(response);
      console.log(this.salarie);
      this.openSnackBar("Enregistré avec succès","undo");
      this.modifyerrorSituation=false;

    },error => {
      console.log(error);
      this.modifyerrorSituation=true;

    })
  }


  modifyUrgentSettings() {
    console.log(this.urgContactFromGroup.value);
    this.salarie.cinUrg = this.urgContactFromGroup.value.cinUrg;
    this.salarie.nomUrg = this.urgContactFromGroup.value.nomUrg;
    this.salarie.prenomUrg = this.urgContactFromGroup.value.prenomUrg;
    this.salarie.emailUrg = this.urgContactFromGroup.value.emailUrg;
    this.salarie.telephoneUrg = this.urgContactFromGroup.value.telephoneUrg;
    this.salarie.adresseUrg = this.urgContactFromGroup.value.adresseUrg;
    this.profileService.modifyProfileContacts(this.salarie).subscribe(response =>{
      console.log(response);
      console.log(this.salarie);
      this.openSnackBar("Enregistré avec succès","undo");
      this.modifyerrorUrgent=false;

    },error => {
      console.log(error);
      this.modifyerrorUrgent=true;

    })
  }

  // *********************** Password ***************************************
  hide :boolean = true;
  checkPasswords(){
    return this.passwordFromGroup.value.newPassword == this.passwordFromGroup.value.confPassword ? this.sendNewPassword() : this.passwordMatchError=true;
  }
  sendNewPassword(){
    this.passwordMatchError=false;
    this.salarie.password=this.passwordFromGroup.value.password;
    this.salarie.newPassword=this.passwordFromGroup.value.newPassword;
    console.log(this.salarie);
    this.settingsService.modifyPassword(this.salarie).subscribe(response => {
      this.passwordWrongError=false;
      this.saved=true;
      this.openSnackBar("Enregistré avec succès","undo");


    },error => {
      console.log(error.error.message);
      this.passwordWrongmessage = error.error.message;
      this.passwordWrongError=true;
      this.passwordMatchError=false;
    })
  }
}
