import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {Salarie} from "../../../models/Salarie";
import {HomeService} from "../../../services/home/home.service";

@Component({
  selector: 'app-support-form',
  templateUrl: './support-form.component.html',
  styleUrls: ['./support-form.component.css']
})
export class SupportFormComponent implements OnInit {
  helpmeFromGroup: FormGroup;
  salarie:Salarie;

  constructor(private homeService:HomeService , @Inject(MAT_DIALOG_DATA) private data :any , private router:Router) {
    this.salarie = data.salarie;
    this.helpmeFromGroup = new FormGroup({
      problem : new FormControl('',Validators.required),
    })
  }

  ngOnInit(): void {
  }

  helpMe() {
    console.log(this.helpmeFromGroup.value);
    this.homeService.getToken().subscribe(resp =>{
      console.log(resp)
    },error => {
      console.log(error)
    })
  }
}
