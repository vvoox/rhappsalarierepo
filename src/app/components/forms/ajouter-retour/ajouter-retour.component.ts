import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CongesService} from "../../../services/conges/conges.service";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Conges} from "../../../models/Conges";
import {Salarie} from "../../../models/Salarie";

@Component({
  selector: 'app-ajouter-retour',
  templateUrl: './ajouter-retour.component.html',
  styleUrls: ['./ajouter-retour.component.css']
})
export class AjouterRetourComponent implements OnInit {

  conge?:Conges;
  salarie:Salarie;
  newConge?:Conges;
  date:any;
  retourFormGroup :FormGroup;

  constructor(private _snackBar :MatSnackBar,private congesService:CongesService , @Inject(MAT_DIALOG_DATA) private data :any)  {
    this.conge = data.conge;
    this.salarie = data.salarie;
  }



  ngOnInit(): void {
    // this.getCongeById();
    console.log("OnInit")
    console.log(this.conge.dateRetour)
    this.retourFormGroup = new FormGroup({
      dateRetour: new FormControl(this.conge.dateRetour, Validators.required),
    })
  }

  // getCongeById(){
  //   this.congesService.getOneConge(this.conge.id).subscribe(response =>{
  //     this.conge = response;
  //   })
  //   }

  addRetour() {
    console.log(this.conge.id)
    console.log(this.retourFormGroup.value.dateRetour)
    this.congesService.addRetourDate(this.conge.id,this.retourFormGroup.value.dateRetour).subscribe(response =>{
      console.log(response);
      this.openSnackBar("Votre déclaration est enregistré avec succès","undo");
    },error => {
      console.log(error);
    })
  }
  openSnackBar(snackBarmessage,snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction,{
      duration: 2000,
    });
  }
}
