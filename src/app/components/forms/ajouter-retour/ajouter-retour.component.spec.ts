import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterRetourComponent } from './ajouter-retour.component';

describe('AjouterRetourComponent', () => {
  let component: AjouterRetourComponent;
  let fixture: ComponentFixture<AjouterRetourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterRetourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterRetourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
