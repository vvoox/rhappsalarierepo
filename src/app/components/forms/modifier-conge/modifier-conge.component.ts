import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CongesService} from "../../../services/conges/conges.service";
import {ProfileService} from "../../../services/profile/profile.service";
import {Router} from "@angular/router";
import {Conges} from "../../../models/Conges";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Salarie} from "../../../models/Salarie";
import {TypeConge} from "../../../models/TypeConge";
import {CongeSalarieRequest} from "../../../models/CongeSalarieRequest";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-modifier-conge',
  templateUrl: './modifier-conge.component.html',
  styleUrls: ['./modifier-conge.component.css']
})
export class ModifierCongeComponent implements OnInit {

  conge : Conges;
  idConge : number;
  title : string;
  salarie:Salarie;
  congeFormGroup : FormGroup;
  typeConges  = ['ANNUEL', 'MARIAGE', 'PACS', 'DECES', 'NAISSANCE'];

  constructor(private _snackBar :MatSnackBar  , @Inject(MAT_DIALOG_DATA) public data: any , private congesService: CongesService,private profileService:ProfileService,private router: Router) {
    this.title = data.title;
    this.conge = data.conge;
    this.salarie = data.salarie;
    console.log(  this.conge );
  }

  ngOnInit(): void {

    this.congeFormGroup = new FormGroup({
      // type : new FormControl(''),
      typeConge : new FormControl(this.conge.type.typeConge,Validators.required),
      dateDebut : new FormControl(this.conge.dateDebut.substring(0,10),Validators.required),
      dateFin : new FormControl(this.conge.dateFin.substring(0,10),Validators.required),
      motif : new FormControl(this.conge.motif,[Validators.required , Validators.minLength(4)]),
    });
  }

  openSnackBar(snackBarmessage,snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction,{
      duration: 2000,
    });
  }
  modifyConge() {
    let cg =new Conges();
    let conge = new CongeSalarieRequest();
    this.conge.dateDebut = this.congeFormGroup.value.dateDebut;
    this.conge.dateFin = this.congeFormGroup.value.dateFin;
    this.conge.motif = this.congeFormGroup.value.motif;
    conge.conge = this.conge
    conge.typeConge = this.congeFormGroup.value.typeConge;
    console.log(conge)
    this.congesService.modifyConge(conge).subscribe(response => {
      console.log(response)
      this.openSnackBar("Enregistré avec succès","undo");

    },error => {
      console.log(error);
    })
  }
}
