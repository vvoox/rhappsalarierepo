import {Component, Inject, OnInit, SystemJsNgModuleLoader} from '@angular/core';
import {CongesService} from "../../../services/conges/conges.service";
import {Router} from "@angular/router";
import {Salarie} from "../../../models/Salarie";
import {ProfileService} from "../../../services/profile/profile.service";
import {CongesComponent} from "../../conges/conges.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Validator} from "@angular/forms";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Conges} from "../../../models/Conges";
import {TypeConge} from "../../../models/TypeConge";
import {CongeSalarieRequest} from "../../../models/CongeSalarieRequest";
import {MatSnackBar} from "@angular/material/snack-bar";


class CongeService {
}

@Component({
  selector: 'app-ajouter-conge',
  templateUrl: './ajouter-conge.component.html',
  styleUrls: ['./ajouter-conge.component.css']
})
export class AjouterCongeComponent implements OnInit {

  // conge : Conges;
  public conge:CongeSalarieRequest;
  public solde:number;
  turnOn:boolean;
  selectedDate: any;
  congesComponent : CongesComponent;
  relaod:any;
  salarie:Salarie;


  email = new FormControl('');
  typeConges  = ['ANNUEL', 'MARIAGE', 'PACS', 'DECES', 'NAISSANCE'];
  // typeConges = [
  //   { type: 'ANNUEL' },
  //   { type: 'MARIAGE' },
  //   {type: 'MARIAGE' },
  //   {type: 'NAISSANCE' },
  //   { type: 'PACS' }];


  congeFormGroup = new FormGroup({
    typeConge : new FormControl('',Validators.required),
    dateDebut : new FormControl('',Validators.required),
    dateFin : new FormControl('',Validators.required),
    motif : new FormControl('',[Validators.required , Validators.minLength(4)]),
  });



  constructor(private _snackBar :MatSnackBar,@Inject(MAT_DIALOG_DATA) private data :any , private congesService: CongesService,private profileService:ProfileService,private router: Router )  {

    this.relaod = data.relaod;
    this.salarie = data.salarie;
    console.log(this.salarie);

  }

  ngOnInit(): void {
    this.profileService.getProfile().subscribe(profile =>{
      // @ts-ignore
      this.salaries=profile;
      // @ts-ignore
      this.solde=this.salaries.solde;
      this.turnOn=true;


    })
  }

  openSnackBar(snackBarmessage,snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction,{
      duration: 2000,
    });
  }


  addConge() {
    console.log(this.congeFormGroup.value)
    let cg =new Conges();
    let conge = new CongeSalarieRequest();
    cg.dateDebut = this.congeFormGroup.value.dateDebut;
    cg.dateFin = this.congeFormGroup.value.dateFin;
    cg.motif = this.congeFormGroup.value.motif;
    conge.conge = cg
    conge.typeConge = this.congeFormGroup.value.typeConge;
    console.log(conge)
    this.congesService.addOneConge(conge).subscribe(data=>{
      console.log(data);
      this.openSnackBar("Votre congé est chargée avec succès","undo");

    },error => {
      console.log(error);
    })
  }

  // turnOffForm() {
  //   this.congesComponent.formAddConge=false;
  //   this.turnOn=this.congesComponent.formAddConge;
  //
  // }

  // ajouterConge() {
  //   console.log(this.congeFormGroup.value);
  //   this.congesService.addOneConge(this.congeFormGroup.value).subscribe(data=>{
  //     this.congesComponent.getConges();
  //     this.turnOffForm();
  //     // this.router.navigateByUrl("/conges");
  //   },error => {
  //     console.log(error);
  //   })
  // }
}
