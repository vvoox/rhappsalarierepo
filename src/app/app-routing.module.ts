import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {RetraitesComponent} from "./components/retraites/retraites.component";
import {AbsencesComponent} from "./components/absences/absences.component";
import {AvantagesComponent} from "./components/avantages/avantages.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {CongesComponent} from "./components/conges/conges.component";
import {SettingsComponent} from "./components/settings/settings.component";
import {LoginComponent} from "./components/login/login.component";
import {ForgetPasswordComponent} from "./components/forget-password/forget-password.component";


const routes: Routes = [
  {
    path:"home",
    component : HomeComponent
  },
  {
    path:"retraites",
    component:RetraitesComponent
  },
  {
    path:"absences",
    component:AbsencesComponent
  },
  {
    path:"avantages",
    component:AvantagesComponent
  },
  {
    path:"profile",
    component:ProfileComponent
  },
  {
    path:"conges",
    component:CongesComponent
  },
  {
    path:"RefreshComponent",component:ProfileComponent
  },
  {
    path:"settings",
    component:SettingsComponent
  },
  {
    path: '', redirectTo: '/login', pathMatch:'full'

  },
  {
    path:"login",component:LoginComponent
  },
  {
    path:"forgot_password",component:ForgetPasswordComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
