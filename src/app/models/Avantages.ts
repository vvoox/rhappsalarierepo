import {Salarie} from "./Salarie";

export class Avantages {

  id:number;
  commission:string
  specification:string
  type:string;
  salarie:Salarie;

}
