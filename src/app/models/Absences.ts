import {Salarie} from "./Salarie";

export class Absences {

  id:number;
  dateDebut?:string;
  dateFin?:string
  justificatif?:string
  dateCreation?:string
  dateModification?:string
  type?:string
  salarie?:Salarie;
  description?:string;

}
