import {Salarie} from "./Salarie";
import {TypeConge} from "./TypeConge";

export class Conges {

  id?:number;
  dateDebut?:string;
  dateFin?:string;
  dateRetour?:string;
  duree?:number;
  motif?:string;
  etat?:string
  dateCreation?:string;
  dateUpdate?:string;
  dateModification?:string;
  type?:TypeConge;
  salarie?:Salarie;

}
