import {Direction} from "./Direction";
import {Service} from "./Service";
import {Salarie} from "./Salarie";

export class Poste {

  nom:string;
  dateCreation?:string;
  dateUpdate?:string;
  dateModification?:string;
  direction:Direction;
  service:Service;
  competences:string[];
  division:string;
  salarie: Salarie;



}
