import {Salarie} from "./Salarie";

export class Retraites {

  id:number;
  dateRetraite?:Date;
  dateValidation?:Date;
  remarques?:string
  etat?:string
  dateCreation?:Date
  dateModification?:Date
  type?:string;
  salarie?:Salarie;

}
