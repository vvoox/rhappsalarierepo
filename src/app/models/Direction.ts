import {Poste} from "./Poste";
import {Salarie} from "./Salarie";
import {Service} from "./Service";

export class Direction {

  nom:string;
  dateCreation?:string;
  dateUpdate?:string;
  poste:Poste[];
  salarie: Salarie[];
  directions:Direction;
  service:Service;

}
