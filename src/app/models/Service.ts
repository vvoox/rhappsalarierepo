import {Poste} from "./Poste";
import {Salarie} from "./Salarie";

export class Service {

  nom:string;
  dateCreation?:string;
  dateUpdate?:string;
  poste:Poste[];
  salarie: Salarie[];


}
