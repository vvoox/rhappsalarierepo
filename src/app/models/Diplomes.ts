import {Salarie} from "./Salarie";

export class Diplomes {

  id:number;
  name:string;
  dateDiplome:Date;
  expDiplome:Date;
  dateCreation:Date;
  dateUpdate:Date;
  path:string;

  salarie?:Salarie

}
