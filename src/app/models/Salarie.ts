import {Service} from "./Service";
import {Direction} from "./Direction";
import {Poste} from "./Poste";
import {Retraites} from "./Retraites";
import {Avantages} from "./Avantages";
import {Absences} from "./Absences";
import {Conges} from "./Conges";
import {Diplomes} from "./Diplomes";

export class Salarie {
  // infos pers

  id: string;
  nom: string;
  prenom: string;
  dateCreation?:string;
  photo: string;
  telephone:string;
  fonction:string;
  solde:string;
  etatFamiliale:string
  nmbEnf:string;
  numSomme: string;
  email: string;
  cin: string;
  cv:string;
  adresse: string;
  dateNaissance: string;
  lieuNaissance: string;
  password:string;
  newPassword:string;

  retraite?:Retraites;
  absences?:Absences[];
  avantages?:Avantages[];
  conges?:Conges[];
  division?: string;
  direction?: Direction;
  service?: Service;
  poste?:Poste;
  diplomeObt: Diplomes[];



  cinUrg?: string;
  nomUrg?: string;
  prenomUrg:string;
  adresseUrg?: string;
  telephoneUrg?: string;
  emailUrg?: string;

}
