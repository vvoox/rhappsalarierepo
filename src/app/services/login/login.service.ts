import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Salarie} from "../../models/Salarie";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginHost = environment.loginHost;

  constructor(private http: HttpClient) { }

  loginIn(salarie:Salarie){
    return this.http.post(this.loginHost+"/auth",salarie);
  }
  forgetPassword(user){
    return this.http.post(this.loginHost+"/forgot_password",user)
  }
}
