import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public host= environment.host;


  constructor(private http:HttpClient) { }

  public modifyPassword(salarie){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.put(this.host + "/profil/modifier/user/password" ,salarie,{headers});
  }
}
