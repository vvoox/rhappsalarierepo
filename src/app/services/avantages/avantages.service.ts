import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AvantagesService {

  public host= environment.host;

  constructor(private http: HttpClient) { }

  public getAllAvantages(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.get(this.host+"/avantages",{headers});
  }
}
