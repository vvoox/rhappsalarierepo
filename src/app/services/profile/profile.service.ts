import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  public host= environment.host;


  constructor(private http: HttpClient) {

  }
  getHeader(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
  }

  public getProfile(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))    // headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"));
    return this.http.get(this.host + "/profil",{headers});
  }

  public modifyProfileUser(profile){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.put(this.host + "/profil/modifier/user" ,profile,{headers});
  }
  public modifyProfileContacts(profile){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.put(this.host + "/profil/modifier/contacts" ,profile,{headers});
  }

  public uploadImage(file:File){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const data: FormData = new FormData();
    data.append('file', file);
    return this.http.post(this.host + "/profil/upload/image",data,{headers});
  }

  public downloadImage(path:string){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const data: FormData = new FormData();
    data.append('pictureName', path);
    return this.http.post(this.host + "/profil/download/image",data,{responseType: "blob",headers });
  }
  public downloadCv(path:string){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const data: FormData = new FormData();
    data.append('cvName', path);
    return this.http.post(this.host + "/profil/download/cv",data,{responseType: "blob",headers});
  }
  public downloadDiplome(path:string){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const data: FormData = new FormData();
    data.append('diplomeName', path);
    return this.http.post(this.host + "/profil/download/diplome",data,{responseType: "blob",headers});
  }

  public uploadCv(file:File){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const cv: FormData = new FormData();
    cv.append('file', file);
    return this.http.post(this.host + "/profil/upload/cv",cv,{headers});
  }

  public uploadDiplome(file:File , data){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const diplome: FormData = new FormData();
    // const data1: FormData = new FormData();
    //tkharbi9a
    let date ;
    console.log(JSON.stringify(data.expDiplome).substring(1,11));
    if(JSON.stringify(data.expDiplome).substring(1,11)=='"'){
      date="";
    }
    else{
      date = JSON.stringify(data.expDiplome).substring(1,11);
    }
    diplome.append('file', file);
    diplome.append('name', data.name);
    diplome.append('dateDiplome', data.dateDiplome)
    diplome.append('expDiplome',date);
    // 2020-05-28

    return this.http.post(this.host + "/profil/upload/diplome",diplome,{headers});
  }


  public deleteCv(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.delete(this.host + "/profil/upload/cv/delete",{headers});
  }
  public deleteDiplome(id){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.delete(this.host + "/profil/upload/diplome/"+id+"/delete",{headers});
  }
  public deleteImage(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.delete(this.host + "/profil/upload/image/delete",{headers});
  }


}
