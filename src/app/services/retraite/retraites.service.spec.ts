import { TestBed } from '@angular/core/testing';

import { RetraitesService } from './retraites.service';

describe('RetraitesService', () => {
  let service: RetraitesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RetraitesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
