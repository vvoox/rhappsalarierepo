import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Subject} from "rxjs";
import {tap} from "rxjs/operators";
import {CongeSalarieRequest} from "../../models/CongeSalarieRequest";

@Injectable({
  providedIn: 'root'
})
export class CongesService {

  public host= environment.host;

  constructor(private http: HttpClient) { }

  private _refreshNeeds$ = new Subject<void>();

  get refreshNeeds$(){
    return this._refreshNeeds$;
  }

  public getAllConges(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.get(this.host+"/conges",{headers});
  }
  public getOneConge(id:number){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
   return this.http.get(this.host+"/conges/"+id,{headers});
  }

  public addOneConge(data){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.post(this.host+"/conges/create",data,{headers})
      .pipe(
        tap((() => {
          this._refreshNeeds$.next();
        })

      )
    )
  }
  public modifyConge(data){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.put(this.host+"/conges/"+data.conge.id+"/modifier",data,{headers})
      .pipe(
      tap((() => {
          this._refreshNeeds$.next();
        })

      )
    )
  }

  public deleteConge(id:number){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.delete(this.host+"/conges/"+id+"/supprimer",{headers})
      .pipe(
      tap((() => {
          this._refreshNeeds$.next();
        })

      )
    )
  }
  public addRetourDate(id,date){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"));
    const data: FormData = new FormData();
    data.append('dateRetour', date);
    return this.http.post(this.host+"/conges/"+id+"/retour",data,{headers})
      .pipe(
        tap((() => {
            this._refreshNeeds$.next();
          })

        )
      )
  }
}
