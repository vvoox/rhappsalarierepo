import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Subject} from "rxjs";
import {tap} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class AbsencesService {

  public host= environment.host;

  private _refreshNeeds$ = new Subject<void>();

  get refreshNeeds$(){
    return this._refreshNeeds$;
  }


  constructor(private http:HttpClient) { }


  public getAbsences(){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.get(this.host+"/absences",{headers});
  }
  public getabsenceById(id){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.get(this.host+"/absences/"+id,{headers});
  }

  // public deleteAbsence(id:number){
  //   return this.httpClient.delete(this.host+"/absences/"+id);
  // }

  public addAbsence(data){
    return this.http.post(this.host+"/absences/create",data);

  }


  public uploadJustif(file:File, id){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const  justif: FormData = new FormData();
    justif.append('file', file);
    justif.append('id', id);
    return this.http.post(this.host + "/absences/"+id+"/upload/justification",justif,{headers});
  }

  public addDescription(id,description:string){
    let headers= new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const  descp: FormData = new FormData();
    descp.append('id',id);
    descp.append('description',description);
    return this.http.post(this.host + "/absences/"+id+"/description",descp,{headers})
      .pipe(
      tap((() => {
          this._refreshNeeds$.next();
        })

      )
    )
  }
  public downloadJustification(path:string) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Bearer " + localStorage.getItem("token"))
    const data: FormData = new FormData();
    data.append('fileName', path);
    return this.http.post(this.host + "/absences/download/justification", data, {responseType: "blob", headers}).pipe(
      tap((() => {
          this._refreshNeeds$.next();
        })

      )
    )
  }


  public deleteJustification(id){
      let headers= new HttpHeaders();
      headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
      return this.http.delete(this.host + "/absences/"+id+"/delete/justification",{headers})
        .pipe(
        tap((() => {
            this._refreshNeeds$.next();
          })

        )
      )
    }
}
